<?php
/**
 * Created by PhpStorm.
 * User: Zedex Info
 * Date: 10-02-2020
 * Time: 07:57 PM
 */

namespace App\Util;
use App\Mail\ForgotPassword;
use App\Mail\Welcome;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Util implements ShouldQueue {
    use Queueable, SerializesModels;

    public static function currentDate(){
        return Carbon::now()->toDateTimeString();
    }

    public static function validationRules(Request $request, $rules){
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation = response()->json($validator->errors(), 422);
            return array_values((array)$validation->getData())[0];
        }
    }

    public static function userRole($user){
        switch ($user) {
            case 1:
                $role = 'Administrator';
                break;
            case 2:
                $role = 'Admin';
                break;
            case 3:
                $role = 'User';
                break;
        }
        return $role;
    }

    public static function SendEmail($parameters){
        try {
            return Mail::to($parameters['email'])->send(new Welcome($parameters));
           /* return [
                'status' => true,
                'message' => 'success',
                'data' => 'Please Check Your Email'
                ];*/
        }catch (\Exception $exception){
            return['status'=>false, 'message'=>$exception->getMessage()];
        }
    }

    public static function pagination($count, $request){
        $page = isset($request->page)?(int)$request->page:1;
        $page_size = isset($request->page_size)?(int)$request->page_size:10;
        return [
            'total_count'   => $count,
            'total_pages'   => ceil($count/$page_size),
            'page'          => $page,
            'page_size'     => $page_size
        ];
    }
}
