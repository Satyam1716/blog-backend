<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Util\Util;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $request;

    public function login(Request $request){
        try{
            $validation = Util::validationRules($request, User::$login_rules);
            if (is_array($validation)) {
                return ['status' => false, 'message' => $validation[0]];
            }
            $parameters = $request->all();
            $user = User::where('email', $parameters["email"])->first();

            if (!$user) {
                return response()->json([
                    'error' => 'Email does not exist.'
                ], 400);
            }

            $user_Detail = self::getLogin($user, $parameters);
            if (!$user_Detail) {
                return ['status' => false, 'message' => 'Wrong password'];
            }
            return $user_Detail;
        }catch (\Exception $exception){
            return['status'=>false, 'message'=>$exception->getMessage()];
        }
    }

    public static function getLogin($user, $credential){
        if (Hash::check($credential["password"], $user->password)) {
            return [
                'status' => true,
                'message' => 'success',
                'data' => [
                    'token' => self::jwt($user),
                    'user_id' => $user->id,
                    'email' => $user->email,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                   // 'role' => Util::userRole($user->role)
                ]
            ];
        }
    }

    public static function jwt(User $user){
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 604800 // Expiration time 7 days
        ];
        return JWT::encode($payload, 'zedex123');
    }
}
