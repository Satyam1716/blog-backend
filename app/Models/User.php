<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'email',
    ];

    protected $hidden = [
        'password',
    ];

    public static $login_rules = [
        'email'     => 'required|email',
        'password'  => 'required'
    ];
}
