<?php

use App\Util\Util;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

        DB::table('users')->insert([
            'first_name' => 'Satyam',
            'last_name' => 'Maurya',
            'username' => 'Satyam1716',
            'email' => 'admin@gmail.com',
            'contact' => '8976353808',
            'password' => Hash::make('1234'),
            'role' => 1,
            'created_at' => Util::currentDate(),
            'updated_at' => Util::currentDate()
        ]);
    }
}
